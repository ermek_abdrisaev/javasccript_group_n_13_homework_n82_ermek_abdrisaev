import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { LockerComponent } from './locker/locker.component';
import { keypadReducer } from '../keypad.reducer';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LockerComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({lock: keypadReducer}, {}),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
