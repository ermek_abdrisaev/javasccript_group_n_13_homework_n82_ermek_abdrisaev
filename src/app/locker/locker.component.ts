import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { addNumber, enter, eraseChar } from '../../keypad.actions';

@Component({
  selector: 'app-locker',
  templateUrl: './locker.component.html',
  styleUrls: ['./locker.component.css']
})
export class LockerComponent {
  lock!: Observable<string>;
  arrPassword!: string[];
  arrayNumbers: string[] = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0'];
  isCorrect!: boolean;

  constructor(
    private store: Store<{lock: string}>) {
    this.lock = store.select('lock');
    this.lock.subscribe( (locker: any) => {
      this.arrPassword = locker.password.split('');
      this.isCorrect = locker.isCorrect;
    })
  }

  enter(){
    this.store.dispatch(enter());
  }
  eraseChar(){
    this.store.dispatch(eraseChar());
  }

  addNumber(number: string){
    this.store.dispatch(addNumber({ind: number.toString()}));
  }
}
