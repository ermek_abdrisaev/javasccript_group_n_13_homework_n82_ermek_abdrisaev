import { createReducer, on } from '@ngrx/store';
import { addNumber, enter, eraseChar } from './keypad.actions';

export const correctPass = '1337';

const initialState = {
  isCorrect: false,
  password: '',
};

export const keypadReducer = createReducer(
  initialState,

  on(enter, (state) => {
    if (state.password === correctPass){
      return {...state, isCorrect: true}
    }
    return {...state, isCorrect: false};
  }),

  on(eraseChar, (state) => {
    if(state.password.length <= 0){
      return state;
    }
    const newPass = state.password.slice(0, state.password.length -1);
    return {...state, password: newPass, isCorrect: false};
  }),

  on(addNumber, (state, {ind}) => {
    const newPass = state.password + ind;
    return {...state, password: newPass};
  })
);
