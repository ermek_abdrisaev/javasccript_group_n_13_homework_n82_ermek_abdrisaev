import { createAction, props } from '@ngrx/store';

export const enter = createAction(
  '[Keypad] Enter code');

export const eraseChar =  createAction(
  '[Keypad] Erase character');

export const addNumber = createAction(
  '[Keypad] addNumber',
  props<{ind: string}>());
